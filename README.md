# Setup

#### Start containers

Start up the containers by using following command. On linux you may require `sudo` to run this command. See google how to fix that.

```sh
docker-compose up -d
```

#### Verify that containers are up by typing

```sh
docker ps

CONTAINER ID        IMAGE                   COMMAND             CREATED             STATUS              PORTS                    NAMES
e98afc4ffefc        node:8                  "node"              3 days ago          Up 3 seconds        0.0.0.0:3000->3000/tcp   swjsgethws_node_1
799c7fd17a5f        swjsgethws_blockchain   "/bin/bash"         3 days ago          Up 2 seconds        0.0.0.0:8545->8545/tcp   swjsgethws_blockchain_1
```

#### Open geth container

Navigate to geth container by typing following command, and see README.md file that is under `blockchain` folder in this projects root.

```sh
docker exec -it %CONTAINER NAME OR ID HERE% /bin/bash
```

you can also create a function in ~/.bash_profile or ~/.bashrc for your use, that will open container by name:

```sh
function container-open {
    echo "\033[0;32mOpening container:\033[0m \n $(container-locate $1)"
    docker exec -it $(docker ps --filter name=$1 -q) /bin/bash
}

function container-locate {
    docker ps --filter name=$1 --format "table {{.ID}}\t{{.Names}}" $2
}
```

#### Open node container

After you've setup your RPC server and ETH accounts navigate to node container and check README.md file under the `source` folder in this projects root.