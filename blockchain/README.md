# Instructions for node setup

#### Init a private node using provided genesis.json file
```sh
geth --datadir ./data/main init genesis.json
```

#### Create accounts for tests
*Alternatively you can create accounts directly from the geth console. See RPC server step below.*
```sh
geth --datadir ./data/main account new
```

#### Start RPC server with console
Here you can provide `--mine` flag, but keep in mind that at least one account on that node should be created.
```sh
geth --rpc --rpcapi="db,eth,net,web3,personal" --rpccorsdomain "http://localhost:3000" --rpcaddr "0.0.0.0" --datadir ./data/main --port 30301 --nodiscover --networkid 123123 console
```

In order to create accounts from geth console you can use `helper.js` file
```sh
loadScript('helper.js');
addAccount();
```
