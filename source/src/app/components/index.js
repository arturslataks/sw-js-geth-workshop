import account from './Account';
import contractCreate from './contracts/Create';
import contractView from './contracts/View';

export default [
    {
        tag: 'account',
        comp: account
    },
    {
        tag: 'contractcreate',
        comp: contractCreate
    },
    {
        tag: 'contractview',
        comp: contractView
    }
];