import Utils from '../utils';

export default {
    state: {
        list: []
    },

    mutations: {
        setNewBalance(state, data) {
            state.list[data.index]['eth'] = data.balance;
        },

        setAccounts(state, list) {
            const accounts = [];

            list.forEach((address) => {
                const account = {
                    address,
                    eth: 0
                }

                accounts.push(account);
            });

            state.list = accounts;
        }
    }
}