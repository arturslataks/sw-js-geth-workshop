import web3 from './web3';
import accounts from './accounts';
import contracts from './contracts';

export default {
    modules: {
        web3,
        accounts,
        contracts
    }
};
