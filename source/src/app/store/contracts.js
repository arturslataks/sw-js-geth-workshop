import Utils from '../utils';

export default {
    state: {
        list: []
    },

    mutations: {
        addContract(state, contract) {
            state.list.push(contract);
        }
    }
}