import Web3 from 'web3';

export default {
    state: {
        inst: null
    },

    mutations: {
        setWeb3(state, web3inst) {
            state.inst = web3inst;
        }
    },

    actions: {
        initWeb3(context) {
            const web3 = new Web3();
            web3.setProvider('http://localhost:8545');

            context.commit('setWeb3', web3);
        }
    }
}