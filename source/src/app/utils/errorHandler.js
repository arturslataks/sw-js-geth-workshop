export default (error) => {
    console.error(error);
    
    if (typeof this.loading !== 'undefined') {
        this.loading = false;
    }

    if (typeof this.error !== 'undefined') {
        this.error = error;
    }

    return error;
}