import Vue from 'vue';
import Vuex from 'vuex';
import Store from './store/';
import Utils from './utils/';
import components from './components';

components.forEach((component) => {
    Vue.component(`bc-${component.tag}`, component.comp);
})

Vue.use(Vuex);
const store = new Vuex.Store(Store);

window.app = new Vue({
    el: '#app',
    store,

    computed: {
        web3() {
            return this.$store.state.web3.inst;
        },

        accounts() {
            return this.$store.state.accounts.list;
        }
    },

    methods: {
        setAccounts() {
            this.web3.eth.getAccounts().then(accounts => {
                this.$store.commit('setAccounts', accounts);
            })
        }
    },

    mounted() {
        this.$store.dispatch('initWeb3');
        this.setAccounts();
    }
});