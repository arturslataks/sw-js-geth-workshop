pragma solidity ^0.4.18;

contract SimpleContract {
    uint constant MAX_UINT = 2**256 - 1;

    uint public price = 5;
    uint public clientBalance = 100;
    uint public ownerBalance = 0;
    address creator = 0x0;

    modifier onlyCreator() {
        require(msg.sender == creator);
        _;
    }

    function SimpleContract(uint _price) public {
        creator = msg.sender;
        price = _price;
    }

    function updatePrice(uint _newPrice) public onlyCreator {
        require(_newPrice > 0 && _newPrice <= MAX_UINT);

        price = _newPrice;
        UpdatePriceEvent(msg.sender, price);
    }

     function buy() public {
        clientBalance -= price;
        ownerBalance += price;
    }

    event UpdatePriceEvent(address indexed _from, uint _value);
}