const webpack = require('webpack');
const path = require('path');
process.noDeprecation = true;

const rootPath = path.join(__dirname, '..', 'src');

module.exports = {
    devtool: 'eval',
    context: path.join(rootPath, 'app'),
    entry: path.join(rootPath, 'app', 'index.js'),

    output: {
        filename: './public/js/app.js'
    },

    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            'utils': path.join(rootPath, 'app', 'utils'),
            'vue$': 'vue/dist/vue.esm.js',
            'contracts': path.join(__dirname, '..', 'build', 'contracts')
        }
    },

    devServer: {
        host: '0.0.0.0',
        contentBase: path.join(__dirname, '..', 'public'),
        compress: true,
        port: 3000
    },

    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },

            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: [/node_modules/],
                options: {
                    presets: ['env']
                }
            }
        ]
    }
}
