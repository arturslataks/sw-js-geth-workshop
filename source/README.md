# Setup

#### Install npm modules

```sh
npm i
```

#### Complie smart contract(s)

```sh
npm run compile
```

#### Start file watcher (to compile js)

```sh
npm run watch
```

#### Start development server

```sh
npm run dev
```